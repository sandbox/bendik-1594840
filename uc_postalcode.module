<?php
/**
 * @file
 * Ubercart postal code module
 */

/**
 * Implements hook_theme().
 */
function uc_postalcode_theme() {
  return array(
    'uc_postalcode_city_label' => array(
      'variables' => array(
        'id'   => NULL,
        'pane' => NULL,
        'city' => NULL,
      ),
    ),
  );
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Adds custom process callbacks on the address fields, and a custom
 * validate callback on the form.
 */
function uc_postalcode_form_uc_cart_checkout_form_alter(&$form, &$form_state) {
  $form_state['uc_ajax']['uc_postalcode']['panes][delivery][address][delivery_postal_code'] = array(
    'uc_postalcode_lookup_callback',
  );
  $form_state['uc_ajax']['uc_postalcode']['panes][billing][address][billing_postal_code'] = array(
    'uc_postalcode_lookup_callback',
  );

  $form['#validate'] = array_merge(array('uc_postalcode_validate'), $form['#validate']);
  
  foreach (array('delivery','billing') as $pane) {
    if (isset($form['panes'][$pane]['address'])) {
      if (!isset($form['panes'][$pane]['address']['#process'])) {
        $form['panes'][$pane]['address']['#process'] = array('uc_store_process_address_field');
      }
      $form['panes'][$pane]['address']['#process'] = array_merge(
        $form['panes'][$pane]['address']['#process'],
        array('uc_postalcode_process_address_field'));
    }
  }
}

/**
 * Adds ajax callbacks to our postal code fields.
 *
 * Hides the city input field and appends a container after the postal code
 * field to hold data from our ajax callback (city name).
 *
 * @param array $element
 *   Element array.
 * @param array $form_state
 *   Form state array.
 */
function uc_postalcode_process_address_field($element, $form_state) {
  $pane = $element['#key_prefix'];
  $element[$pane . '_city']['#type'] = 'hidden';
  $city = $element[$pane . '_city']['#default_value'];
  $id = 'uc-postalcode-' . $pane . '-city';
  $element[$pane . '_postal_code']['#field_suffix'] = theme('uc_postalcode_city_label',
    array('id' => $id, 'city' => $city, 'pane' => $pane));

  return $element;
}

/**
 * AJAX callback for the postal code field.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array.
 */
function uc_postalcode_lookup_callback($form, &$form_state) {
  $pane = $form_state['triggering_element']['#array_parents'][1];
  $country_id = $form_state['values']['panes'][$pane][$pane . '_country'];
  $postal_code = $form_state['values']['panes'][$pane][$pane . '_postal_code'];
  $location = _uc_postalcode_bring_callback($postal_code, $country_id);

  $input_city_name = 'panes[' . $pane . '][' . $pane . '_city]';
  $input_city = theme_hidden(
    array(
      'element' => array(
        '#name'  => $input_city_name,
        '#value' => $location ? $location : '',
      ),
  ));
  $id = 'uc-postalcode-' . $pane . '-city';
  $city_label = theme('uc_postalcode_city_label', array(
    'id' => $id,
    'city' => $location,
    'pane' => $pane,
  ));

  $commands = array();
  $commands[] = ajax_command_replace('input[name="' . $input_city_name . '"]', $input_city);
  $commands[] = ajax_command_replace('#' . $id, $city_label);
  return array('#type' => 'ajax', '#commands' => $commands);
}


/**
 * Validates the form, making sure the postal code is valid.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array.
 */
function uc_postalcode_validate($form, &$form_state) {
  foreach (array('delivery', 'billing') as $pane) {
    $postal_code = $form_state['values']['panes'][$pane][$pane . '_postal_code'];
    $country_id = $form_state['values']['panes'][$pane][$pane . '_country'];

    $location = _uc_postalcode_bring_callback($postal_code, $country_id);
    if (!$location) {
      form_set_error('panes][' . $pane . '][' . $pane . '_postal_code', t('Invalid postal code'));
      $location = '';
    }
    $fake_element = array('#parents' => array('panes', $pane, $pane . '_city'));
    form_set_value($fake_element, $location, $form_state);
  }
}


/**
 * Theme function to format HTML containing city name.
 *
 * @param array $variables
 *   Contains two keys:
 *   - id: HTML element id
 *   - pane: The pane name (billing or delivery)
 *   - city: The location returned from Bring callback - or false
 */
function theme_uc_postalcode_city_label($variables) {
  $id   = $variables['id'];
  $pane = $variables['pane'];
  $city = $variables['city'];
  if ($city === FALSE) {
    $city = t('Invalid postal code');
  }
  return format_string('<span id="@id">@city</span>', array(
    '@id' => $id,
    '@city' => $city,
  ));
}


/**
 * Perform the request to Bring`s API.
 *
 * @param string $postal_code
 *   The postal code.
 * @param int $country_id
 *   country_id, as found in uc_countries.
 *
 * @return string
 *   The corresponding location, or FALSE if not found.
 */
function _uc_postalcode_bring_callback($postal_code, $country_id) {
  $country = uc_get_country_data(array('country_id' => $country_id));
  $country_iso_code_2 = $country[0]['country_iso_code_2'];

  $bring_url = 'http://fraktguide.bring.no/fraktguide/api/postalCode.json?';
  $bring_url .= 'country=' . strtolower($country_iso_code_2);
  $bring_url .= '&pnr=' . $postal_code;
  $result = drupal_http_request($bring_url);
  if ($result->code != 200) {
    watchdog('uc_postalcode', 'Request to Bring API failed with error code @errcode (@errmsg): @errdata', array(
      '@errcode' => $result->code,
      '@errmsg'  => $result->error,
      '@errdata' => $result->data), WATCHDOG_ERROR);
    return FALSE;
  }
  $response = json_decode($result->data);
  if ($response->valid) {
    return drupal_ucfirst(drupal_strtolower($response->result));
  }
  return FALSE;
}
