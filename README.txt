=============================
Ubercart Postal Code
=============================


About this module
-----------------

uc_postalcode is a tiny module which looks up and validates postal codes
entered in Ubercart`s checkout form.
It will make the city input field hidden, and instead provide the city
name after the postal code input.

Requirements
-------------

This module requires Ubercart version 3.

Installation & Configuration
----------------------------

Install as any other Drupal module. 
Make sure the postal code and city fields is enabled:
admin/store/settings/countries/fields.
